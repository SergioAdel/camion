<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230313064441 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE assurance_engin (id INT AUTO_INCREMENT NOT NULL, engin_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, duree INT NOT NULL, INDEX IDX_AEB174B7E58AF0C2 (engin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chauffeur_engin (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, date_dembauche DATE NOT NULL, permis VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE controle_technique_engin (id INT AUTO_INCREMENT NOT NULL, engin_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, duree INT NOT NULL, INDEX IDX_3EA956AFE58AF0C2 (engin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE engin (id INT AUTO_INCREMENT NOT NULL, chauffeur_id INT NOT NULL, matricule VARCHAR(255) NOT NULL, marque VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, annee DATE NOT NULL, INDEX IDX_1FA4CE0485C0B3BE (chauffeur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visite_engin (id INT AUTO_INCREMENT NOT NULL, engin_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, duree INT NOT NULL, INDEX IDX_A34F3CB1E58AF0C2 (engin_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assurance_engin ADD CONSTRAINT FK_AEB174B7E58AF0C2 FOREIGN KEY (engin_id) REFERENCES engin (id)');
        $this->addSql('ALTER TABLE controle_technique_engin ADD CONSTRAINT FK_3EA956AFE58AF0C2 FOREIGN KEY (engin_id) REFERENCES engin (id)');
        $this->addSql('ALTER TABLE engin ADD CONSTRAINT FK_1FA4CE0485C0B3BE FOREIGN KEY (chauffeur_id) REFERENCES chauffeur_engin (id)');
        $this->addSql('ALTER TABLE visite_engin ADD CONSTRAINT FK_A34F3CB1E58AF0C2 FOREIGN KEY (engin_id) REFERENCES engin (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE assurance_engin DROP FOREIGN KEY FK_AEB174B7E58AF0C2');
        $this->addSql('ALTER TABLE controle_technique_engin DROP FOREIGN KEY FK_3EA956AFE58AF0C2');
        $this->addSql('ALTER TABLE engin DROP FOREIGN KEY FK_1FA4CE0485C0B3BE');
        $this->addSql('ALTER TABLE visite_engin DROP FOREIGN KEY FK_A34F3CB1E58AF0C2');
        $this->addSql('DROP TABLE assurance_engin');
        $this->addSql('DROP TABLE chauffeur_engin');
        $this->addSql('DROP TABLE controle_technique_engin');
        $this->addSql('DROP TABLE engin');
        $this->addSql('DROP TABLE visite_engin');
    }
}
