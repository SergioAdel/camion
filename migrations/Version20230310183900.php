<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230310183900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE assurance_camion (id INT AUTO_INCREMENT NOT NULL, trucks_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, duree INT NOT NULL, INDEX IDX_F0C91609B6BC7EE (trucks_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE camion (id INT AUTO_INCREMENT NOT NULL, matricule VARCHAR(255) NOT NULL, marque VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, annee DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chauffeur (id INT AUTO_INCREMENT NOT NULL, cami_id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, date_d_embauche DATE NOT NULL, formation VARCHAR(255) NOT NULL, INDEX IDX_5CA777B85A1642AE (cami_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE controle_technique (id INT AUTO_INCREMENT NOT NULL, trucks_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, duree INT NOT NULL, INDEX IDX_1DA6481D9B6BC7EE (trucks_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visite_technique (id INT AUTO_INCREMENT NOT NULL, trucks_id INT NOT NULL, debut DATE NOT NULL, fin DATE NOT NULL, duree INT NOT NULL, INDEX IDX_348A54D79B6BC7EE (trucks_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE assurance_camion ADD CONSTRAINT FK_F0C91609B6BC7EE FOREIGN KEY (trucks_id) REFERENCES camion (id)');
        $this->addSql('ALTER TABLE chauffeur ADD CONSTRAINT FK_5CA777B85A1642AE FOREIGN KEY (cami_id) REFERENCES camion (id)');
        $this->addSql('ALTER TABLE controle_technique ADD CONSTRAINT FK_1DA6481D9B6BC7EE FOREIGN KEY (trucks_id) REFERENCES camion (id)');
        $this->addSql('ALTER TABLE visite_technique ADD CONSTRAINT FK_348A54D79B6BC7EE FOREIGN KEY (trucks_id) REFERENCES camion (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE assurance_camion DROP FOREIGN KEY FK_F0C91609B6BC7EE');
        $this->addSql('ALTER TABLE chauffeur DROP FOREIGN KEY FK_5CA777B85A1642AE');
        $this->addSql('ALTER TABLE controle_technique DROP FOREIGN KEY FK_1DA6481D9B6BC7EE');
        $this->addSql('ALTER TABLE visite_technique DROP FOREIGN KEY FK_348A54D79B6BC7EE');
        $this->addSql('DROP TABLE assurance_camion');
        $this->addSql('DROP TABLE camion');
        $this->addSql('DROP TABLE chauffeur');
        $this->addSql('DROP TABLE controle_technique');
        $this->addSql('DROP TABLE visite_technique');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
