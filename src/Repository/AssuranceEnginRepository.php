<?php

namespace App\Repository;

use App\Entity\AssuranceEngin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AssuranceEngin>
 *
 * @method AssuranceEngin|null find($id, $lockMode = null, $lockVersion = null)
 * @method AssuranceEngin|null findOneBy(array $criteria, array $orderBy = null)
 * @method AssuranceEngin[]    findAll()
 * @method AssuranceEngin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssuranceEnginRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssuranceEngin::class);
    }

    public function add(AssuranceEngin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AssuranceEngin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return AssuranceEngin[] Returns an array of AssuranceEngin objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?AssuranceEngin
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
