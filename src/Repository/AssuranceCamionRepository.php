<?php

namespace App\Repository;

use App\Entity\AssuranceCamion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AssuranceCamion>
 *
 * @method AssuranceCamion|null find($id, $lockMode = null, $lockVersion = null)
 * @method AssuranceCamion|null findOneBy(array $criteria, array $orderBy = null)
 * @method AssuranceCamion[]    findAll()
 * @method AssuranceCamion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssuranceCamionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssuranceCamion::class);
    }

    public function add(AssuranceCamion $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AssuranceCamion $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return AssuranceCamion[] Returns an array of AssuranceCamion objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?AssuranceCamion
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
