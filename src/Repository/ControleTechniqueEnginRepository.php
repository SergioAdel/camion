<?php

namespace App\Repository;

use App\Entity\ControleTechniqueEngin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ControleTechniqueEngin>
 *
 * @method ControleTechniqueEngin|null find($id, $lockMode = null, $lockVersion = null)
 * @method ControleTechniqueEngin|null findOneBy(array $criteria, array $orderBy = null)
 * @method ControleTechniqueEngin[]    findAll()
 * @method ControleTechniqueEngin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ControleTechniqueEnginRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ControleTechniqueEngin::class);
    }

    public function add(ControleTechniqueEngin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ControleTechniqueEngin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ControleTechniqueEngin[] Returns an array of ControleTechniqueEngin objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ControleTechniqueEngin
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
