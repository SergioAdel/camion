<?php

namespace App\Entity;

use App\Entity\Engin;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use App\Repository\ChauffeurEnginRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=ChauffeurEnginRepository::class)
 */
class ChauffeurEngin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephone;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDembauche;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $permis;

    /**
     * @ORM\OneToMany(targetEntity=Engin::class, mappedBy="chauffeur")
     */
    private $engins;

    public function __construct()
    {
        $this->engins = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getDateDembauche(): ?\DateTimeInterface
    {
        return $this->dateDembauche;
    }

    public function setDateDembauche(\DateTimeInterface $dateDembauche): self
    {
        $this->dateDembauche = $dateDembauche;

        return $this;
    }

    public function getPermis(): ?string
    {
        return $this->permis;
    }

    public function setPermis(string $permis): self
    {
        $this->permis = $permis;

        return $this;
    }

    /**
     * @return Collection<int, Engin>
     */
    public function getEngins(): Collection
    {
        return $this->engins;
    }

    public function addEngin(Engin $engin): self
    {
        if (!$this->engins->contains($engin)) {
            $this->engins[] = $engin;
            $engin->setChauffeur($this);
        }

        return $this;
    }

    public function removeEngin(Engin $engin): self
    {
        if ($this->engins->removeElement($engin)) {
            // set the owning side to null (unless already changed)
            if ($engin->getChauffeur() === $this) {
                $engin->setChauffeur(null);
            }
        }

        return $this;
    }
}
