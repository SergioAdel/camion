<?php

namespace App\Entity;

use App\Entity\VisiteEngin;
use App\Entity\AssuranceEngin;
use App\Entity\ChauffeurEngin;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EnginRepository;
use App\Entity\ControleTechniqueEngin;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=EnginRepository::class)
 */
class Engin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * @ORM\Column(type="date")
     */
    private $annee;

    /**
     * @ORM\OneToMany(targetEntity=AssuranceEngin::class, mappedBy="engin")
     */
    private $assuranceEngins;

    /**
     * @ORM\OneToMany(targetEntity=VisiteEngin::class, mappedBy="engin")
     */
    private $visiteEngins;

    /**
     * @ORM\OneToMany(targetEntity=ControleTechniqueEngin::class, mappedBy="engin")
     */
    private $controleTechniqueEngins;

    /**
     * @ORM\ManyToOne(targetEntity=ChauffeurEngin::class, inversedBy="engins")
     * @ORM\JoinColumn(nullable=false)
     */
    private $chauffeur;

    public function __construct()
    {
        $this->assuranceEngins = new ArrayCollection();
        $this->visiteEngins = new ArrayCollection();
        $this->controleTechniqueEngins = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getAnnee(): ?\DateTimeInterface
    {
        return $this->annee;
    }

    public function setAnnee(\DateTimeInterface $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * @return Collection<int, AssuranceEngin>
     */
    public function getAssuranceEngins(): Collection
    {
        return $this->assuranceEngins;
    }

    public function addAssuranceEngin(AssuranceEngin $assuranceEngin): self
    {
        if (!$this->assuranceEngins->contains($assuranceEngin)) {
            $this->assuranceEngins[] = $assuranceEngin;
            $assuranceEngin->setEngin($this);
        }

        return $this;
    }

    public function removeAssuranceEngin(AssuranceEngin $assuranceEngin): self
    {
        if ($this->assuranceEngins->removeElement($assuranceEngin)) {
            // set the owning side to null (unless already changed)
            if ($assuranceEngin->getEngin() === $this) {
                $assuranceEngin->setEngin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, VisiteEngin>
     */
    public function getVisiteEngins(): Collection
    {
        return $this->visiteEngins;
    }

    public function addVisiteEngin(VisiteEngin $visiteEngin): self
    {
        if (!$this->visiteEngins->contains($visiteEngin)) {
            $this->visiteEngins[] = $visiteEngin;
            $visiteEngin->setEngin($this);
        }

        return $this;
    }

    public function removeVisiteEngin(VisiteEngin $visiteEngin): self
    {
        if ($this->visiteEngins->removeElement($visiteEngin)) {
            // set the owning side to null (unless already changed)
            if ($visiteEngin->getEngin() === $this) {
                $visiteEngin->setEngin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ControleTechniqueEngin>
     */
    public function getControleTechniqueEngins(): Collection
    {
        return $this->controleTechniqueEngins;
    }

    public function addControleTechniqueEngin(ControleTechniqueEngin $controleTechniqueEngin): self
    {
        if (!$this->controleTechniqueEngins->contains($controleTechniqueEngin)) {
            $this->controleTechniqueEngins[] = $controleTechniqueEngin;
            $controleTechniqueEngin->setEngin($this);
        }

        return $this;
    }

    public function removeControleTechniqueEngin(ControleTechniqueEngin $controleTechniqueEngin): self
    {
        if ($this->controleTechniqueEngins->removeElement($controleTechniqueEngin)) {
            // set the owning side to null (unless already changed)
            if ($controleTechniqueEngin->getEngin() === $this) {
                $controleTechniqueEngin->setEngin(null);
            }
        }

        return $this;
    }

    public function getChauffeur(): ?chauffeurEngin
    {
        return $this->chauffeur;
    }

    public function setChauffeur(?chauffeurEngin $chauffeur): self
    {
        $this->chauffeur = $chauffeur;

        return $this;
    }
}
