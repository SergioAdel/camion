<?php

namespace App\Entity;

use App\Entity\Engin;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\AssuranceEnginRepository;

/**
 * @ORM\Entity(repositoryClass=AssuranceEnginRepository::class)
 */
class AssuranceEngin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $debut;

    /**
     * @ORM\Column(type="date")
     */
    private $fin;

    /**
     * @ORM\Column(type="integer")
     */
    private $duree;

    /**
     * @ORM\ManyToOne(targetEntity=Engin::class, inversedBy="assuranceEngins")
     * @ORM\JoinColumn(nullable=false)
     */
    private $engin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getDuree(): ?int
    {
        return $this->duree;
    }

    public function setDuree(int $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getEngin(): ?engin
    {
        return $this->engin;
    }

    public function setEngin(?engin $engin): self
    {
        $this->engin = $engin;

        return $this;
    }
}
