<?php

namespace App\Entity;

use App\Repository\CamionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CamionRepository::class)
 */
class Camion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $annee;

    /**
     * @ORM\OneToMany(targetEntity=Chauffeur::class, mappedBy="cami")
     */
    private $chauffeurs;

    /**
     * @ORM\OneToMany(targetEntity=ControleTechnique::class, mappedBy="trucks")
     */
    private $controleTechniques;

    /**
     * @ORM\OneToMany(targetEntity=VisiteTechnique::class, mappedBy="trucks")
     */
    private $visiteTechniques;

    /**
     * @ORM\OneToMany(targetEntity=AssuranceCamion::class, mappedBy="trucks")
     */
    private $assuranceCamions;

    public function __construct()
    {
        $this->chauffeurs = new ArrayCollection();
        $this->controleTechniques = new ArrayCollection();
        $this->visiteTechniques = new ArrayCollection();
        $this->assuranceCamions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getAnnee(): ?\DateTimeInterface
    {
        return $this->annee;
    }

    public function setAnnee(?\DateTimeInterface $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * @return Collection<int, Chauffeur>
     */
    public function getChauffeurs(): Collection
    {
        return $this->chauffeurs;
    }

    public function addChauffeur(Chauffeur $chauffeur): self
    {
        if (!$this->chauffeurs->contains($chauffeur)) {
            $this->chauffeurs[] = $chauffeur;
            $chauffeur->setCami($this);
        }

        return $this;
    }

    public function removeChauffeur(Chauffeur $chauffeur): self
    {
        if ($this->chauffeurs->removeElement($chauffeur)) {
            // set the owning side to null (unless already changed)
            if ($chauffeur->getCami() === $this) {
                $chauffeur->setCami(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ControleTechnique>
     */
    public function getControleTechniques(): Collection
    {
        return $this->controleTechniques;
    }

    public function addControleTechnique(ControleTechnique $controleTechnique): self
    {
        if (!$this->controleTechniques->contains($controleTechnique)) {
            $this->controleTechniques[] = $controleTechnique;
            $controleTechnique->setTrucks($this);
        }

        return $this;
    }

    public function removeControleTechnique(ControleTechnique $controleTechnique): self
    {
        if ($this->controleTechniques->removeElement($controleTechnique)) {
            // set the owning side to null (unless already changed)
            if ($controleTechnique->getTrucks() === $this) {
                $controleTechnique->setTrucks(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, VisiteTechnique>
     */
    public function getVisiteTechniques(): Collection
    {
        return $this->visiteTechniques;
    }

    public function addVisiteTechnique(VisiteTechnique $visiteTechnique): self
    {
        if (!$this->visiteTechniques->contains($visiteTechnique)) {
            $this->visiteTechniques[] = $visiteTechnique;
            $visiteTechnique->setTrucks($this);
        }

        return $this;
    }

    public function removeVisiteTechnique(VisiteTechnique $visiteTechnique): self
    {
        if ($this->visiteTechniques->removeElement($visiteTechnique)) {
            // set the owning side to null (unless already changed)
            if ($visiteTechnique->getTrucks() === $this) {
                $visiteTechnique->setTrucks(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AssuranceCamion>
     */
    public function getAssuranceCamions(): Collection
    {
        return $this->assuranceCamions;
    }

    public function addAssuranceCamion(AssuranceCamion $assuranceCamion): self
    {
        if (!$this->assuranceCamions->contains($assuranceCamion)) {
            $this->assuranceCamions[] = $assuranceCamion;
            $assuranceCamion->setTrucks($this);
        }

        return $this;
    }

    public function removeAssuranceCamion(AssuranceCamion $assuranceCamion): self
    {
        if ($this->assuranceCamions->removeElement($assuranceCamion)) {
            // set the owning side to null (unless already changed)
            if ($assuranceCamion->getTrucks() === $this) {
                $assuranceCamion->setTrucks(null);
            }
        }

        return $this;
    }
}
