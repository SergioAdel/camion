<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CamController extends AbstractController
{
    /**
     * @Route("/cam", name="app_cam")
     */
    public function index(): Response
    {
        return $this->render('cam/index.html.twig');
    }
}
