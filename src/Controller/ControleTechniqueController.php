<?php

namespace App\Controller;

use App\Entity\ControleTechnique;
use App\Form\ControleTechniqueType;
use App\Repository\ControleTechniqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/controle/technique")
 */
class ControleTechniqueController extends AbstractController
{
    /**
     * @Route("/", name="app_controle_technique_index", methods={"GET"})
     */
    public function index(ControleTechniqueRepository $controleTechniqueRepository): Response
    {
        return $this->render('controle_technique/index.html.twig', [
            'controle_techniques' => $controleTechniqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_controle_technique_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ControleTechniqueRepository $controleTechniqueRepository): Response
    {
        $controleTechnique = new ControleTechnique();
        $form = $this->createForm(ControleTechniqueType::class, $controleTechnique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $controleTechniqueRepository->add($controleTechnique, true);

            return $this->redirectToRoute('app_controle_technique_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controle_technique/new.html.twig', [
            'controle_technique' => $controleTechnique,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_controle_technique_show", methods={"GET"})
     */
    public function show(ControleTechnique $controleTechnique): Response
    {
        return $this->render('controle_technique/show.html.twig', [
            'controle_technique' => $controleTechnique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_controle_technique_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ControleTechnique $controleTechnique, ControleTechniqueRepository $controleTechniqueRepository): Response
    {
        $form = $this->createForm(ControleTechniqueType::class, $controleTechnique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $controleTechniqueRepository->add($controleTechnique, true);

            return $this->redirectToRoute('app_controle_technique_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controle_technique/edit.html.twig', [
            'controle_technique' => $controleTechnique,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_controle_technique_delete", methods={"POST"})
     */
    public function delete(Request $request, ControleTechnique $controleTechnique, ControleTechniqueRepository $controleTechniqueRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$controleTechnique->getId(), $request->request->get('_token'))) {
            $controleTechniqueRepository->remove($controleTechnique, true);
        }

        return $this->redirectToRoute('app_controle_technique_index', [], Response::HTTP_SEE_OTHER);
    }
}
