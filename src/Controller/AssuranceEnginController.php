<?php

namespace App\Controller;

use App\Entity\AssuranceEngin;
use App\Form\AssuranceEnginType;
use App\Repository\AssuranceEnginRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/assurance/engin")
 */
class AssuranceEnginController extends AbstractController
{
    /**
     * @Route("/", name="app_assurance_engin_index", methods={"GET"})
     */
    public function index(AssuranceEnginRepository $assuranceEnginRepository): Response
    {
        return $this->render('assurance_engin/index.html.twig', [
            'assurance_engins' => $assuranceEnginRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_assurance_engin_new", methods={"GET", "POST"})
     */
    public function new(Request $request, AssuranceEnginRepository $assuranceEnginRepository): Response
    {
        $assuranceEngin = new AssuranceEngin();
        $form = $this->createForm(AssuranceEnginType::class, $assuranceEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $assuranceEnginRepository->add($assuranceEngin, true);

            return $this->redirectToRoute('app_assurance_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('assurance_engin/new.html.twig', [
            'assurance_engin' => $assuranceEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_assurance_engin_show", methods={"GET"})
     */
    public function show(AssuranceEngin $assuranceEngin): Response
    {
        return $this->render('assurance_engin/show.html.twig', [
            'assurance_engin' => $assuranceEngin,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_assurance_engin_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, AssuranceEngin $assuranceEngin, AssuranceEnginRepository $assuranceEnginRepository): Response
    {
        $form = $this->createForm(AssuranceEnginType::class, $assuranceEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $assuranceEnginRepository->add($assuranceEngin, true);

            return $this->redirectToRoute('app_assurance_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('assurance_engin/edit.html.twig', [
            'assurance_engin' => $assuranceEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_assurance_engin_delete", methods={"POST"})
     */
    public function delete(Request $request, AssuranceEngin $assuranceEngin, AssuranceEnginRepository $assuranceEnginRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$assuranceEngin->getId(), $request->request->get('_token'))) {
            $assuranceEnginRepository->remove($assuranceEngin, true);
        }

        return $this->redirectToRoute('app_assurance_engin_index', [], Response::HTTP_SEE_OTHER);
    }
}
