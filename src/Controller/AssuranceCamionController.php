<?php

namespace App\Controller;

use App\Entity\AssuranceCamion;
use App\Form\AssuranceCamionType;
use App\Repository\AssuranceCamionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/assurance/camion")
 */
class AssuranceCamionController extends AbstractController
{
    /**
     * @Route("/", name="app_assurance_camion_index", methods={"GET"})
     */
    public function index(AssuranceCamionRepository $assuranceCamionRepository): Response
    {
        return $this->render('assurance_camion/index.html.twig', [
            'assurance_camions' => $assuranceCamionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_assurance_camion_new", methods={"GET", "POST"})
     */
    public function new(Request $request, AssuranceCamionRepository $assuranceCamionRepository): Response
    {
        $assuranceCamion = new AssuranceCamion();
        $form = $this->createForm(AssuranceCamionType::class, $assuranceCamion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $assuranceCamionRepository->add($assuranceCamion, true);

            return $this->redirectToRoute('app_assurance_camion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('assurance_camion/new.html.twig', [
            'assurance_camion' => $assuranceCamion,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_assurance_camion_show", methods={"GET"})
     */
    public function show(AssuranceCamion $assuranceCamion): Response
    {
        return $this->render('assurance_camion/show.html.twig', [
            'assurance_camion' => $assuranceCamion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_assurance_camion_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, AssuranceCamion $assuranceCamion, AssuranceCamionRepository $assuranceCamionRepository): Response
    {
        $form = $this->createForm(AssuranceCamionType::class, $assuranceCamion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $assuranceCamionRepository->add($assuranceCamion, true);

            return $this->redirectToRoute('app_assurance_camion_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('assurance_camion/edit.html.twig', [
            'assurance_camion' => $assuranceCamion,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_assurance_camion_delete", methods={"POST"})
     */
    public function delete(Request $request, AssuranceCamion $assuranceCamion, AssuranceCamionRepository $assuranceCamionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$assuranceCamion->getId(), $request->request->get('_token'))) {
            $assuranceCamionRepository->remove($assuranceCamion, true);
        }

        return $this->redirectToRoute('app_assurance_camion_index', [], Response::HTTP_SEE_OTHER);
    }
}
