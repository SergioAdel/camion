<?php

namespace App\Controller;

use App\Entity\VisiteTechnique;
use App\Form\VisiteTechniqueType;
use App\Repository\VisiteTechniqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/visite/technique")
 */
class VisiteTechniqueController extends AbstractController
{
    /**
     * @Route("/", name="app_visite_technique_index", methods={"GET"})
     */
    public function index(VisiteTechniqueRepository $visiteTechniqueRepository): Response
    {
        return $this->render('visite_technique/index.html.twig', [
            'visite_techniques' => $visiteTechniqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_visite_technique_new", methods={"GET", "POST"})
     */
    public function new(Request $request, VisiteTechniqueRepository $visiteTechniqueRepository): Response
    {
        $visiteTechnique = new VisiteTechnique();
        $form = $this->createForm(VisiteTechniqueType::class, $visiteTechnique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $visiteTechniqueRepository->add($visiteTechnique, true);

            return $this->redirectToRoute('app_visite_technique_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('visite_technique/new.html.twig', [
            'visite_technique' => $visiteTechnique,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_visite_technique_show", methods={"GET"})
     */
    public function show(VisiteTechnique $visiteTechnique): Response
    {
        return $this->render('visite_technique/show.html.twig', [
            'visite_technique' => $visiteTechnique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_visite_technique_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, VisiteTechnique $visiteTechnique, VisiteTechniqueRepository $visiteTechniqueRepository): Response
    {
        $form = $this->createForm(VisiteTechniqueType::class, $visiteTechnique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $visiteTechniqueRepository->add($visiteTechnique, true);

            return $this->redirectToRoute('app_visite_technique_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('visite_technique/edit.html.twig', [
            'visite_technique' => $visiteTechnique,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_visite_technique_delete", methods={"POST"})
     */
    public function delete(Request $request, VisiteTechnique $visiteTechnique, VisiteTechniqueRepository $visiteTechniqueRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$visiteTechnique->getId(), $request->request->get('_token'))) {
            $visiteTechniqueRepository->remove($visiteTechnique, true);
        }

        return $this->redirectToRoute('app_visite_technique_index', [], Response::HTTP_SEE_OTHER);
    }
}
