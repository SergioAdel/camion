<?php

namespace App\Controller;

use App\Entity\VisiteEngin;
use App\Form\VisiteEnginType;
use App\Repository\VisiteEnginRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/visite/engin")
 */
class VisiteEnginController extends AbstractController
{
    /**
     * @Route("/", name="app_visite_engin_index", methods={"GET"})
     */
    public function index(VisiteEnginRepository $visiteEnginRepository): Response
    {
        return $this->render('visite_engin/index.html.twig', [
            'visite_engins' => $visiteEnginRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_visite_engin_new", methods={"GET", "POST"})
     */
    public function new(Request $request, VisiteEnginRepository $visiteEnginRepository): Response
    {
        $visiteEngin = new VisiteEngin();
        $form = $this->createForm(VisiteEnginType::class, $visiteEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $visiteEnginRepository->add($visiteEngin, true);

            return $this->redirectToRoute('app_visite_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('visite_engin/new.html.twig', [
            'visite_engin' => $visiteEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_visite_engin_show", methods={"GET"})
     */
    public function show(VisiteEngin $visiteEngin): Response
    {
        return $this->render('visite_engin/show.html.twig', [
            'visite_engin' => $visiteEngin,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_visite_engin_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, VisiteEngin $visiteEngin, VisiteEnginRepository $visiteEnginRepository): Response
    {
        $form = $this->createForm(VisiteEnginType::class, $visiteEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $visiteEnginRepository->add($visiteEngin, true);

            return $this->redirectToRoute('app_visite_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('visite_engin/edit.html.twig', [
            'visite_engin' => $visiteEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_visite_engin_delete", methods={"POST"})
     */
    public function delete(Request $request, VisiteEngin $visiteEngin, VisiteEnginRepository $visiteEnginRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$visiteEngin->getId(), $request->request->get('_token'))) {
            $visiteEnginRepository->remove($visiteEngin, true);
        }

        return $this->redirectToRoute('app_visite_engin_index', [], Response::HTTP_SEE_OTHER);
    }
}
