<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoteController extends AbstractController
{
    /**
     * @Route("/cote", name="app_cote")
     */
    public function index(): Response
    {
        return $this->render('cote/index.html.twig');
    }
}
