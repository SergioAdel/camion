<?php

namespace App\Controller;

use App\Entity\ChauffeurEngin;
use App\Form\ChauffeurEnginType;
use App\Repository\ChauffeurEnginRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chauffeur-engin")
 */
class ChauffeurEnginController extends AbstractController
{
    /**
     * @Route("/", name="app_chauffeur_engin_index", methods={"GET"})
     */
    public function index(ChauffeurEnginRepository $chauffeurEnginRepository): Response
    {
        return $this->render('chauffeur_engin/index.html.twig', [
            'chauffeur_engins' => $chauffeurEnginRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_chauffeur_engin_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ChauffeurEnginRepository $chauffeurEnginRepository): Response
    {
        $chauffeurEngin = new ChauffeurEngin();
        $form = $this->createForm(ChauffeurEnginType::class, $chauffeurEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $chauffeurEnginRepository->add($chauffeurEngin, true);

            return $this->redirectToRoute('app_chauffeur_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('chauffeur_engin/new.html.twig', [
            'chauffeur_engin' => $chauffeurEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_chauffeur_engin_show", methods={"GET"})
     */
    public function show(ChauffeurEngin $chauffeurEngin): Response
    {
        return $this->render('chauffeur_engin/show.html.twig', [
            'chauffeur_engin' => $chauffeurEngin,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_chauffeur_engin_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ChauffeurEngin $chauffeurEngin, ChauffeurEnginRepository $chauffeurEnginRepository): Response
    {
        $form = $this->createForm(ChauffeurEnginType::class, $chauffeurEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $chauffeurEnginRepository->add($chauffeurEngin, true);

            return $this->redirectToRoute('app_chauffeur_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('chauffeur_engin/edit.html.twig', [
            'chauffeur_engin' => $chauffeurEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_chauffeur_engin_delete", methods={"POST"})
     */
    public function delete(Request $request, ChauffeurEngin $chauffeurEngin, ChauffeurEnginRepository $chauffeurEnginRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$chauffeurEngin->getId(), $request->request->get('_token'))) {
            $chauffeurEnginRepository->remove($chauffeurEngin, true);
        }

        return $this->redirectToRoute('app_chauffeur_engin_index', [], Response::HTTP_SEE_OTHER);
    }
}
