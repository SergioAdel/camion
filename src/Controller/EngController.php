<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EngController extends AbstractController
{
    /**
     * @Route("/eng", name="app_eng")
     */
    public function index(): Response
    {
        return $this->render('eng/index.html.twig', [
            'controller_name' => 'EngController',
        ]);
    }
}
