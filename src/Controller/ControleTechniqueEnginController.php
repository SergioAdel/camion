<?php

namespace App\Controller;

use App\Entity\ControleTechniqueEngin;
use App\Form\ControleTechniqueEnginType;
use App\Repository\ControleTechniqueEnginRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/controle/technique/engin")
 */
class ControleTechniqueEnginController extends AbstractController
{
    /**
     * @Route("/", name="app_controle_technique_engin_index", methods={"GET"})
     */
    public function index(ControleTechniqueEnginRepository $controleTechniqueEnginRepository): Response
    {
        return $this->render('controle_technique_engin/index.html.twig', [
            'controle_technique_engins' => $controleTechniqueEnginRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_controle_technique_engin_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ControleTechniqueEnginRepository $controleTechniqueEnginRepository): Response
    {
        $controleTechniqueEngin = new ControleTechniqueEngin();
        $form = $this->createForm(ControleTechniqueEnginType::class, $controleTechniqueEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $controleTechniqueEnginRepository->add($controleTechniqueEngin, true);

            return $this->redirectToRoute('app_controle_technique_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controle_technique_engin/new.html.twig', [
            'controle_technique_engin' => $controleTechniqueEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_controle_technique_engin_show", methods={"GET"})
     */
    public function show(ControleTechniqueEngin $controleTechniqueEngin): Response
    {
        return $this->render('controle_technique_engin/show.html.twig', [
            'controle_technique_engin' => $controleTechniqueEngin,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_controle_technique_engin_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ControleTechniqueEngin $controleTechniqueEngin, ControleTechniqueEnginRepository $controleTechniqueEnginRepository): Response
    {
        $form = $this->createForm(ControleTechniqueEnginType::class, $controleTechniqueEngin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $controleTechniqueEnginRepository->add($controleTechniqueEngin, true);

            return $this->redirectToRoute('app_controle_technique_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('controle_technique_engin/edit.html.twig', [
            'controle_technique_engin' => $controleTechniqueEngin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_controle_technique_engin_delete", methods={"POST"})
     */
    public function delete(Request $request, ControleTechniqueEngin $controleTechniqueEngin, ControleTechniqueEnginRepository $controleTechniqueEnginRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$controleTechniqueEngin->getId(), $request->request->get('_token'))) {
            $controleTechniqueEnginRepository->remove($controleTechniqueEngin, true);
        }

        return $this->redirectToRoute('app_controle_technique_engin_index', [], Response::HTTP_SEE_OTHER);
    }
}
