<?php

namespace App\Controller;

use App\Entity\Engin;
use App\Form\EnginType;
use App\Repository\EnginRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/engin")
 */
class EnginController extends AbstractController
{
    /**
     * @Route("/", name="app_engin_index", methods={"GET"})
     */
    public function index(EnginRepository $enginRepository): Response
    {
        return $this->render('engin/index.html.twig', [
            'engins' => $enginRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_engin_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EnginRepository $enginRepository): Response
    {
        $engin = new Engin();
        $form = $this->createForm(EnginType::class, $engin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $enginRepository->add($engin, true);

            return $this->redirectToRoute('app_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('engin/new.html.twig', [
            'engin' => $engin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_engin_show", methods={"GET"})
     */
    public function show(Engin $engin): Response
    {
        return $this->render('engin/show.html.twig', [
            'engin' => $engin,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_engin_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Engin $engin, EnginRepository $enginRepository): Response
    {
        $form = $this->createForm(EnginType::class, $engin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $enginRepository->add($engin, true);

            return $this->redirectToRoute('app_engin_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('engin/edit.html.twig', [
            'engin' => $engin,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_engin_delete", methods={"POST"})
     */
    public function delete(Request $request, Engin $engin, EnginRepository $enginRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$engin->getId(), $request->request->get('_token'))) {
            $enginRepository->remove($engin, true);
        }

        return $this->redirectToRoute('app_engin_index', [], Response::HTTP_SEE_OTHER);
    }
}
